cd ~
git clone https://github.com/AlexeyAB/darknet
cd darknet
mkdir build_release
cd build_release
cmake ..
cmake --build . --target install --parallel 8

# по итогу в папке  ~/darknet/build_release/
# появится бинарник darknet