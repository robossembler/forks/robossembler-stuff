### Object detection для шахматных фигур

Папка 'detection' требуется в runtime для навыка 'object detection' на основе [darknet-YOLOv4](https://github.com/AlexeyAB/darknet).
Для получения darknet из исходников нужно запустить darknet_build.sh, и в папке darknet/build_release/ появится бинарник, который нужно скопировать в нашу папку 'detection'.  
Для тестирования можно запустить команду run_test.sh, после выполнения которой должен появиться файл с предсказанием позиций фигур: prediction.jpg.